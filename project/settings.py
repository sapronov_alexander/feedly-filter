# -*- encoding: utf-8 -*-

import os

PROJECT_DIR = os.path.normpath(os.path.abspath(
    os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)))

FEEDLY_TOKEN = ''

RES_FOLDER = os.path.join(PROJECT_DIR, 'resources')
FILE_WORDS_CASE = os.path.join(RES_FOLDER, 'words_case.txt')
FILE_WORDS_REGEXP = os.path.join(RES_FOLDER, 'words_regexp.txt')
TMP_TIME_FILE = 'tmp.time'

try:
    from .local_settings import *
except ImportError as e:
    print(e)
