# -*- encoding: utf-8 -*-
import json
import os

from project.settings import RES_FOLDER, FILE_WORDS_CASE



def main():
    filepath = os.path.join(RES_FOLDER, 'extension_json.txt')
    with open(FILE_WORDS_CASE, 'r') as fio:
        words = [x.strip() for x in fio.readlines()]

    with open(filepath, 'r') as fio:
        data = json.loads(fio.read())

    words.extend([x[0].strip() for x in eval(data.get('Filters'))])
    words = list(set(words))
    words = sorted(words)

    with open(FILE_WORDS_CASE, 'w') as fio:
        fio.write('\n'.join(words))

if __name__ == '__main__':
    main()
