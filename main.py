# -*- encoding: utf-8 -*-

from functools import partial
import datetime
import os
import time
import re
from functools import reduce
import string

from feedly.client import FeedlyClient
import lxml.html as lh

from project.settings import FEEDLY_TOKEN, FILE_WORDS_CASE, FILE_WORDS_REGEXP, \
    TMP_TIME_FILE, PROJECT_DIR


def _get_word_text(text):
    text = reduce(lambda s, c: s.replace(c, ''), string.punctuation, text)
    return text.split()


def checker_no_case(words, text):
    for word in _get_word_text(text):
        try:
            if word.lower() in words:
                return False
        except UnicodeError as e:
            if word.encode('utf-8').lower() in words:
                return False

    return True


def checker_regex(words, text):
    for word in _get_word_text(text):
        for x in words:
            m = re.search(r"%s" % x, word)
            if m is not None:
                return False
    return True


def read_file(filepath):
    with open(filepath, 'r') as f:
        result = f.readlines()
        result = [x.lower().strip() for x in result]
        return result


def get_articles(feedly, categories, previous_time):
    result = []
    for i, category in enumerate(categories):
        if type(category) != str:
            print_str = "{} of {} category ({})".format(
                i + 1,
                len(categories),
                category.get('label'))
            try:
                print(print_str)
            except UnicodeEncodeError as e:
                print(print_str.encode('utf-8'))
            else:
                pass
            if category.get('label') != 'Youtube':
                _ = feedly.get_feed_content(feedly.token,
                                                 category.get('id'),
                                                 True,
                                                 previous_time)
                items = _.get('items', [])
                result.extend(items)
    return result


def get_time_previous_run(filepath: str) -> datetime.datetime:
    try:
        with open(filepath, 'r') as fio:
            return fio.read().strip()
    except FileNotFoundError:
        return None


def save_current_time(filepath: str) -> None:
    with open(filepath, 'w') as fio:
        dt = datetime.datetime.now()
        fio.write(str(int(time.mktime(dt.timetuple()))))


def filter_rss(token, tmp_tile_path):
    checkers = [partial(checker_no_case, [read_file(FILE_WORDS_CASE)]),
                partial(checker_regex, [read_file(FILE_WORDS_REGEXP)])]

    feedly = FeedlyClient(token=token, sandbox=False)
    _cats = feedly.get_info_type(token, 'categories')  # categories
    articles = get_articles(feedly, _cats, get_time_previous_run(tmp_tile_path))
    save_current_time(tmp_tile_path)

    mark_ids = []
    for item in articles:
        try:
            # url = item.get('alternate')[0].get('href')
            title = item.get('title', '')
            description = item.get('summary', {}).get('content')
            if description is None:
                description = ''
            else:
                description = lh.fromstring(description).text_content().strip()
        except TypeError as e:
            print(e, description, title)

        for checker in checkers:
            if not checker(description) or not checker(title):
                mark_ids.append(item['id'])

    mark_ids = list(set(mark_ids))
    if mark_ids:
        feedly.mark_article_read(token, mark_ids)

    with open(os.path.join(PROJECT_DIR, 'results.out'), 'a') as f:
        f.write("%s - %s\n" % (datetime.datetime.now(), len(mark_ids)))


filter_rss(FEEDLY_TOKEN, TMP_TIME_FILE)
